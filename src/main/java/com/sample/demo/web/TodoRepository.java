package com.sample.demo.web;

import com.sample.demo.Todo;

import java.util.List;

public interface TodoRepository {

    void save(Todo todo);

    List<Todo> findAll();

}
