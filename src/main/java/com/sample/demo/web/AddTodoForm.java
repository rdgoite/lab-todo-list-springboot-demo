package com.sample.demo.web;

import com.sample.demo.Todo;

public class AddTodoForm {

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Todo getTodo() {
        return new Todo(description);
    }

}
