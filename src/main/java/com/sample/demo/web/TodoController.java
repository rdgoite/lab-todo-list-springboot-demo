package com.sample.demo.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/todolist")
public class TodoController {

    @Autowired
    private TodoRepository todoRepository;

    @GetMapping
    public String showTodoList(Model model) {
        model.addAttribute("todoList", todoRepository.findAll());
        return "todo/list";
    }

    @PostMapping
    public String addTodo(@ModelAttribute AddTodoForm form) {
        todoRepository.save(form.getTodo());
        return "redirect:/todolist";
    }

}
