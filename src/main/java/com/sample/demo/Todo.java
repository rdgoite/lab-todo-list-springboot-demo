package com.sample.demo;

public class Todo {

    private final String id;

    private final String description;

    public Todo(String description) {
        this("", description);
    }

    public Todo(String id, String description) {
        this.id = id;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

}
