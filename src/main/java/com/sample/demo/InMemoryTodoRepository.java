package com.sample.demo;

import com.sample.demo.web.TodoRepository;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class InMemoryTodoRepository implements TodoRepository {

    private List<Todo> todoList = new CopyOnWriteArrayList<>();

    @Override
    public void save(Todo todo) {
        todoList.add(new Todo(UUID.randomUUID().toString().toUpperCase(), todo.getDescription()));
    }

    @Override
    public List<Todo> findAll() {
        return Collections.unmodifiableList(todoList);
    }

}
