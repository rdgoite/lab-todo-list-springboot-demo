package com.sample.demo;

import com.sample.demo.web.TodoRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TodoListApplication {

    @Bean
    protected TodoRepository todoRepository() {
        return new InMemoryTodoRepository();
    }

	public static void main(String[] args) {
		SpringApplication.run(TodoListApplication.class, args);
	}

}
