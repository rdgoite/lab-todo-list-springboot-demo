package com.sample.demo.web;

import com.sample.demo.Todo;
import com.sample.demo.web.TodoController;
import org.assertj.core.api.Assertions;
import org.assertj.core.groups.Tuple;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers={TodoController.class})
public class TodoControllerTest {

    @Autowired
    private MockMvc webApp;

    @MockBean
    private TodoRepository todoRepository;

    @Before
    public void setUp() {
        reset(todoRepository);
    }

    @Test
    public void testShowList() throws Exception {
        //given:
        List<Todo> todoList = asList(new Todo("efc392", "clean room"),
                new Todo("c12bb0", "organize books"));
        doReturn(todoList).when(todoRepository).findAll();

        //when:
        MvcResult result = webApp.perform(get("/todolist"))
                .andReturn();

        //then:
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

        //and:
        ModelAndView modelAndView = result.getModelAndView();
        assertThat(modelAndView.getViewName()).isEqualTo("todo/list");

        //and:
        List<Todo> modelList = (List<Todo>) modelAndView.getModel().get("todoList");
        assertThat(modelList)
                .hasSameSizeAs(todoList)
                .extracting("id", "description")
                .contains(todoList.stream()
                        .map(todo -> tuple(todo.getId(), todo.getDescription()))
                        .collect(Collectors.toList())
                        .toArray(new Tuple[todoList.size()])
                );
    }

    @Test
    public void testAddTodo() throws Exception {
        //when:
        String description = "grocery shopping";
        MvcResult result = webApp.perform(post("/todolist")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("description", description))
                .andReturn();

        //then:
        assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.FOUND.value());

        //and:
        assertThat(result.getResponse().getRedirectedUrl()).isEqualTo("/todolist");

        //and:
        ArgumentCaptor<Todo> todoCaptor = ArgumentCaptor.forClass(Todo.class);
        verify(todoRepository).save(todoCaptor.capture());

        //and:
        assertThat(todoCaptor.getValue().getDescription()).isEqualTo(description);
    }

}